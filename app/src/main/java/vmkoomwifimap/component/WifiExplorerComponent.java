package vmkoomwifimap.component;

import javax.inject.Singleton;

import dagger.Component;
import vmkoomfreewifi.application.AppController;
import vmkoomfreewifi.main.MainAppActivity_v2;
import vmkoomwifimap.activity.MapActivity;
import vmkoomwifimap.module.DaggerModule;

/**
 * Created by Federico
 */
@Singleton
@Component(modules = DaggerModule.class)
public interface WifiExplorerComponent {
    void inject(AppController application);
    void inject(MainAppActivity_v2 mainActivity);
    void inject(MapActivity mapActivity);
}

package vmkoomwifimap.utils;


import com.vmkoom.wifimap.connection.hotspot.wifianalyzer.password.anywhere.R;

import javax.inject.Inject;

import vmkoomwifimap.model.db.DataBaseHandler;
import vmkoomwifimap.model.wifi.WifiElement;
import vmkoomwifimap.model.wifi.WifiKeeper;

/**
 * Created by Federico
 */
public class ResourceProvider {
    private final WifiKeeper wifiKeeper;
    private final DataBaseHandler dataBaseHandler;

    @Inject
    public ResourceProvider(WifiKeeper wifiKeeper, DataBaseHandler dataBaseHandler) {
        this.wifiKeeper = wifiKeeper;
        this.dataBaseHandler = dataBaseHandler;
    }

    public int getWifiResource(WifiElement wifiElement) {
        if (wifiKeeper.contains(wifiElement.getBSSID()) && wifiElement.isLineOfSight()) {
            return wifiElement.isSecure()
                    ? WifiSecureImageEnum.values()[wifiElement.getSignalLevel()].getResource()
                    : WifiImageEnum.values()[wifiElement.getSignalLevel()].getResource();
        }

        return R.drawable.signal_wifi1;
    }

    public int getSavedResource() {
        return R.drawable.signal_close;
    }

    public int getSavedResource(WifiElement wifiElement) {
        return dataBaseHandler.contains(wifiElement)
                ? R.drawable.signal_close
                : R.drawable.signal_wifi4;
    }
}

package vmkoomwifimap.activity;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.vmkoom.wifimap.connection.hotspot.wifianalyzer.password.anywhere.R;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import vmkoomfreewifi.application.AppController;
import utils.AppUtils;
import vmkoomwifimap.activity.dialog.WifiLocationDialog;
import vmkoomwifimap.activity.dialog.WifiSecureDialog;
import vmkoomwifimap.model.db.DataBaseHandler;
import vmkoomwifimap.model.location.LocationHandler;
import vmkoomwifimap.model.location.LocationKeeper;
import vmkoomwifimap.model.wifi.WifiElement;
import vmkoomwifimap.model.wifi.WifiKeeper;
import vmkoomwifimap.model.wifi.container.strategy.sortedlist.WifiList;
import vmkoomwifimap.ui.presenter.FilterDelegate;

/**
 * Created by Federico
 */
public class MapActivity extends AppCompatActivity implements
        OnMapReadyCallback, WifiLocationDialog.WifiLocationDialogListener,
        WifiSecureDialog.WifiSecureDialogListener {
    private FrameLayout adContainerView;
    private AdView adtView;

    public static final String EXTRA_WIFI_FAVOURITES = "wifi_list";
    private static final int ZOOM_LEVEL = 18;

    private GoogleMap map;
    private boolean favouritesMap;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    WifiKeeper wifiKeeper;

    @Inject
    LocationHandler locationHandler;

    @Inject
    DataBaseHandler dataBaseHandler;

    @Inject
    FilterDelegate filterDelegate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_map);

        initializeInjectors();
        initializeViewComponents();

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });
        //Init admob or facebook
        initAds();
    }

    private void initializeInjectors() {
        ButterKnife.bind(this);
        ((AppController) getApplication()).getComponent().inject(this);
    }

    private void initializeViewComponents() {
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.activity_map_title);
        }

        favouritesMap = getIntent().getBooleanExtra(EXTRA_WIFI_FAVOURITES, false);
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;

        if (favouritesMap)
            renderMap(map, dataBaseHandler.getList());
        else
            renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_map_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                finish();
                onBackPressed();
                return true;

            case R.id.map_filter_location:
                DialogFragment wifiLocationDialog = new WifiLocationDialog();
                wifiLocationDialog.show(getSupportFragmentManager(), "WifiLocationDialog");
                return true;

            case R.id.map_filter_secure:
                DialogFragment wifiSecureDialog = new WifiSecureDialog();
                wifiSecureDialog.show(getSupportFragmentManager(), "WifiLocationDialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogNearbyClick(DialogFragment dialog) {
        filterDelegate.showNearbyWifiList();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogSessionClick(DialogFragment dialog) {
        filterDelegate.showSessionWifiList();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogFavouriteClick(DialogFragment dialog) {
        renderMap(map, dataBaseHandler.getList());
    }

    @Override
    public void onDialogOpenNetworksClick(DialogFragment dialog) {
        filterDelegate.showOnlyOpenNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogSecureNetworksClick(DialogFragment dialog) {
        filterDelegate.showOnlyClosedNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    @Override
    public void onDialogAllNetworksClick(DialogFragment dialog) {
        filterDelegate.showAllNetworks();
        renderMap(map, wifiKeeper.getFilteredList());
    }

    private void renderMap(GoogleMap map, WifiList wifiList) {

        if(map == null) return;

        map.clear();
        map.setMyLocationEnabled(true);

        final LatLng currentLatLng = locationHandler.getCurrentLatLng();
        if (currentLatLng != null) {
            final CameraUpdate locationCamera = CameraUpdateFactory.newLatLngZoom(currentLatLng, ZOOM_LEVEL);
            map.animateCamera(locationCamera);
        }

        for(WifiElement wifiElement : wifiList) {
            LocationKeeper locationKeeper = locationHandler.get(wifiElement.getBSSID());

            if (locationKeeper == null)
                continue;

            LatLng center = locationKeeper.getCenter().getLocation();

            map.addMarker(new MarkerOptions().position(center).title(wifiElement.getSSID()));

            CircleOptions options = new CircleOptions();
            options.center(center);
            options.radius(locationKeeper.getCenter().getRadius());
            options.fillColor(wifiElement.getLightColor());
            options.strokeColor(wifiElement.getBoldColor());
            options.strokeWidth(5);

            map.addCircle(options);
        }
    }

//    private com.facebook.ads.AdView adViewfacebook;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);
            adContainerView = findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(this,adViewContainer);

        }
    }
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (adtView != null) {
            adtView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (adtView != null) {
            adtView.resume();
        }
    }
    @Override
    public void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
        if (adtView != null) {
            adtView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }

        super.onBackPressed();
    }
    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(this);
        adtView.setAdUnitId(getResources().getString(R.string.admob_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }
}
package vmkoomwifimap.model.wifi.container.strategy;

import java.util.List;

import vmkoomwifimap.model.wifi.WifiElement;
import vmkoomwifimap.model.wifi.container.strategy.sortedlist.WifiList;

/**
 * Created by Federico
 */
public class CurrentWifiList extends WifiList implements WifiListPopulate {
    @Override
    public void populate(List<WifiElement> wifiElementList) {
        clear();
        addAll(wifiElementList);
    }
}

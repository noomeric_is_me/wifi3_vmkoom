package vmkoomwifimap.model.wifi.container.strategy;

import java.util.List;

import vmkoomwifimap.model.wifi.WifiElement;

/**
 * Created by Federico
 */
public interface WifiListPopulate {
    void populate(List<WifiElement> wifiElementList);
}

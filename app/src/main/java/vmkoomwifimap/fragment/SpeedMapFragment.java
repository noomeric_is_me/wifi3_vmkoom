package vmkoomwifimap.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
//import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.vmkoom.wifimap.connection.hotspot.wifianalyzer.password.anywhere.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import vmkoomfreewifi.application.AppController;
import vmkoomfreewifi.main.MapsMarkerActivity;
import vmkoomfreewifi.services.GsonRequest;
import vmkoomfreewifi.services.ServiceAPI;
import vmkoomfreewifi.services.model.External_IP;
import vmkoomfreewifi.services.model.NetworkInformation;
import vmkoommainapp.scrollable.fragment.FragmentPagerFragment;
import utils.AppUtils;
import vmkoomwifimap.activity.MapActivity;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by VMKoom on 8/12/2017 AD.
 */

public class SpeedMapFragment extends FragmentPagerFragment {

    public static final String TAG = SpeedMapFragment.class.getSimpleName();
    private TextView network_name,wifi_name,linkspeed,external_ip_address,internal_ip_address,mac_address,security_bssid
            ,ip_city,ip_region,ip_country,ip_latitude,ip_longitude,ip_time_zone,ip_postal_code,ip_asn,ip_org;
    private View mView;
    //    private PublisherAdView mPublisherAdView;
    private NestedScrollView mScrollView;
    private FrameLayout adContainerView;
    private AdView adtView;
    public static SpeedMapFragment newInstance()
    {
        return new SpeedMapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mView == null){
            mView = inflater.inflate(R.layout.fragment_speed_map, container, false);

            mScrollView = mView.findViewById(R.id.scrollView);
            ip_city = (TextView) mView.findViewById(R.id.ip_city);
            ip_region = (TextView) mView.findViewById(R.id.ip_region);
            ip_country = (TextView) mView.findViewById(R.id.ip_country);
            ip_latitude = (TextView) mView.findViewById(R.id.ip_latitude);
            ip_longitude = (TextView) mView.findViewById(R.id.ip_longitude);
            ip_time_zone = (TextView) mView.findViewById(R.id.ip_time_zone);
            ip_postal_code = (TextView) mView.findViewById(R.id.ip_postal_code);
            ip_asn = (TextView) mView.findViewById(R.id.ip_asn);
            ip_org = (TextView) mView.findViewById(R.id.ip_org);
//            TextView text_title_network_map = (TextView) mView.findViewById(R.id.text_title_network_map);
//            text_title_network_map.setCompoundDrawablesWithIntrinsicBounds(
//                    R.drawable.ic_zoom_out_map_black_24dp, 0, 0, 0);

            ImageView wifi_map_zoom = (ImageView) mView.findViewById(R.id.wifi_map_zoom);
            wifi_map_zoom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Network Map Location Zoom - Image Click"));

                    if(mLat == 0 || mLng ==0){
                        return;
                    }

                    openMapsMarker();
                }
            });

            LinearLayout linear_wifi_map = (LinearLayout) mView.findViewById(R.id.linear_wifi_map);
            linear_wifi_map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Network Map Location Zoom - Text Click"));

                    if(mLat == 0 || mLng ==0){
                        return;
                    }

                    openMapsMarker();
                }
            });

            TextView txt_wifi_map_explorer = (TextView) mView.findViewById(R.id.txt_wifi_map_explorer);
            txt_wifi_map_explorer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Wi-Fi Map Explorer - Text Click"));

                    openMapsExplorer();

                }
            });
//
//            CardView txt_wifi_speed_test = (CardView) mView.findViewById(R.id.txt_wifi_speed_test);
//            txt_wifi_speed_test.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    final Intent intent;
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Wi-Fi Map Explorer - Text Click"));
//
//                    intent = new Intent(getActivity(), SpeedTestMiniActivity.class);
//                    startActivity(intent);
//
//                }
//            });
//
//            TextView txt_wifi_map_explorer = (TextView) mView.findViewById(R.id.txt_wifi_map_explorer);
//            txt_wifi_map_explorer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Answers.getInstance().logContentView(new ContentViewEvent()
//                            .putContentName("Wi-Fi Map Explorer - Text Click"));
//
//                    openMapsExplorer();
//
//                }
//            });

//            MobileAds.initialize(getActivity(), getResources().getString(R.string.admob_app_id));
            refreshAd();
            initAds();

        }

        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //Get Network information
        getNetworkInformation();
    }

    @Override
    public void onDestroy() {
        //if(adViewfacebook != null){
        //    adViewfacebook.destroy();
        //}
        if (adtView != null) {
            adtView.destroy();
        }
        super.onDestroy();
    }

    //    private com.facebook.ads.AdView adViewfacebook;
    //    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Banner
//            AdView mAdView = (AdView) mView.findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);
            adContainerView = mView.findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
//            mPublisherAdView = mView.findViewById(R.id.admob_banner_view);
//            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.setVisibility(View.VISIBLE);
//            mPublisherAdView.loadAd(adRequest);

//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view);
//            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest);

//            mPublisherAdView = (PublisherAdView) mView.findViewById(R.id.fluid_view_1);
//            PublisherAdRequest publisherAdRequest_1 = new PublisherAdRequest.Builder().build();
//            mPublisherAdView.loadAd(publisherAdRequest_1);
            //load large ads native bottom
//            NativeExpressAdView adViewNative = (NativeExpressAdView) mView.findViewById(R.id.adViewNative);
//            adViewNative.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative);

//            NativeExpressAdView adViewNative_1 = (NativeExpressAdView) mView.findViewById(R.id.adViewNative_1);
//            adViewNative_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showNativeAdsBanner(adViewNative_1);
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Banner
            RelativeLayout adViewContainer = (RelativeLayout) mView.findViewById(R.id.facebook_banner_ad_container);
            adViewContainer.setVisibility(View.VISIBLE);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(getActivity(),adViewContainer);

            //Native
            LinearLayout adNativeViewContainer = (LinearLayout) mView.findViewById(R.id.speed_map_native_ad_small_container);
            adNativeViewContainer.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showFBAdsNativeSmallInit(getActivity(),adNativeViewContainer);

            LinearLayout adNativeViewContainer_1 = (LinearLayout) mView.findViewById(R.id.speed_map_native_ad_container);
            adNativeViewContainer_1.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showFBAdsNativeInit(getActivity(),adNativeViewContainer_1);

        }
    }

    private void getNetworkInformation() {

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getExternal_IP() == null) {
            getExternal_IP();
        }


        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }else {
            double lat = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
            double lng = Double.parseDouble(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
            setGoogleMap(lat,lng);
        }

        if(AppUtils.getInstance().getAppNetworkInfo() == null || AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation() == null) {
            getNetworkInfo();
        }else {
            ip_city.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getCity());
            ip_region.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getRegionName());
            ip_country.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getCountry());
            ip_latitude.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLat());
            ip_longitude.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getLon());
            ip_time_zone.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getTimezone());
            ip_postal_code.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getZip());
            ip_asn.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getAs());
            ip_org.setText(AppUtils.getInstance().getAppNetworkInfo().getNetworkInformation().getOrg());
        }
    }


    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);

                    double lat = Double.parseDouble(response.getLat());
                    double lng = Double.parseDouble(response.getLon());
                    setGoogleMap(lat,lng);

                    ip_city.setText(response.getCity());
                    ip_region.setText(response.getRegionName());
                    ip_country.setText(response.getCountry());
                    ip_latitude.setText(response.getLat());
                    ip_longitude.setText(response.getLon());
                    ip_time_zone.setText(response.getTimezone());
                    ip_postal_code.setText(response.getZip());
                    ip_asn.setText(response.getAs());
                    ip_org.setText(response.getOrg());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void getExternal_IP(){

        String get_external_ip_url = "https://api.ipify.org?format=json";
        //Volley
        GsonRequest reqExternalIp = ServiceAPI.getExternalIP(get_external_ip_url,new Response.Listener<External_IP>() {
            @Override
            public void onResponse(External_IP response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setExternal_IP(response);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqExternalIp.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqExternalIp, TAG);
    }

    public String getMacAddress() {
        try {
            List<NetworkInterface> networkInterfaceList = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : networkInterfaceList) {
                if (!networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    continue;
                }

                byte[] macAddress = networkInterface.getHardwareAddress();
                if (macAddress == null) {
                    return "";
                }

                StringBuilder result = new StringBuilder();
                for (byte data : macAddress) {
                    result.append(Integer.toHexString(data & 0xFF)).append(":");
                }

                if (result.length() > 0) {
                    result.deleteCharAt(result.length() - 1);
                }
                return result.toString();
            }
        } catch (Exception e) {
        }
        return "02:00:00:00:00:00";
    }

    /** Get IP For mobile */
    public static String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipaddress = inetAddress .getHostAddress().toString();
                        return ipaddress;
                    }
                }
            }
        } catch (SocketException ex) {
            FirebaseCrashlytics.getInstance().recordException(ex);
        }
        return null;
    }


    private double mLat, mLng;
    private void setGoogleMap(final double lat, final double lng){
        mLat = lat;
        mLng = lng;

        if(isAdded()){
//            try {
//                SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//                mapFragment.getMapAsync(new OnMapReadyCallback() {
//                    @Override
//                    public void onMapReady(GoogleMap googleMap) {
//
//                        LatLng sydney = new LatLng(lat, lng);
//                        googleMap.addMarker(new MarkerOptions().position(sydney).title("Network Here!"));
//                        //        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
//                    }
//                });
//            } catch (Exception e) {
//                FirebaseCrashlytics.getInstance().recordException(e);
//            }

            try {

                //https://maps.googleapis.com/maps/api/staticmap?center=13.7768,100.579&zoom=12&size=400x400&key=AIzaSyDBMlJ1ukpfqHfmVbYUd-lGtty7lRkY_3A
                ImageView imageView = (ImageView) mView.findViewById(R.id.wifi_map_static);
                String static_map = "https://maps.googleapis.com/maps/api/staticmap?center=&markers=size:mid%7Ccolor:red%7Clabel:N%7C"
                        + mLat
                        + ","
                        + mLng
                        + "&zoom=17&size=640x640&maptype=roadmap&key="
                        + AppController.getInstance().getAppContext().getString(R.string.google_static_map_api_key);
                Glide.with(this).load(static_map).into(imageView);

            } catch (Exception e) {
                FirebaseCrashlytics.getInstance().recordException(e);
            }
        }
    }

    @Override
    public boolean canScrollVertically(int direction) {
        return mScrollView != null && mScrollView.canScrollVertically(direction);
    }

    @Override
    public void onFlingOver(int y, long duration) {
        if (mScrollView != null) {
            mScrollView.smoothScrollBy(0, y);
        }
    }

    private void openMapsExplorer(){
        try {

//            if (AppUtils.ads_interstitial_show_all) {
//
//                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                    AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                    startActivity(intent);
//                }
//
//            } else {
//
//                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                    AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                    AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
//                    startActivity(intent);
//                }
//            }

            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapActivity.class);
            startActivity(intent);

        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void openMapsMarker(){
        try {

//            if (AppUtils.ads_interstitial_show_all) {
//
//                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
//                    AppUtils.getInstance().showAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                    startActivity(intent);
//                }
//
//            } else {
//
//                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
//                    AppUtils.getInstance().showAdmobAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
//                    AppUtils.getInstance().showFBAdsFullBanner(new AppUtils.BaseAdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                            startActivity(intent);
//                        }
//                    });
//                } else {
//
//                    Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
//                    intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
//                    intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
//                    startActivity(intent);
//                }
//            }

            Intent intent = new Intent(AppController.getInstance().getAppContext(), MapsMarkerActivity.class);
            intent.putExtra(MapsMarkerActivity.LAT_MESSAGE, mLat);
            intent.putExtra(MapsMarkerActivity.LNG_MESSAGE, mLng);
            startActivity(intent);

        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }




//
//    /**
//     * Populates a {@link UnifiedNativeAdView} object with data from a given
//     * {@link UnifiedNativeAd}.
//     *
//     * @param nativeAd the object containing the ad's assets
//     * @param adView          the view to be populated
//     */
//    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
//        try {
//            // Get the video controller for the ad. One will always be provided, even if the ad doesn't
//            // have a video asset.
//            VideoController vc = nativeAd.getVideoController();
//
//            // Create a new VideoLifecycleCallbacks object and pass it to the VideoController. The
//            // VideoController will call methods on this object when events occur in the video
//            // lifecycle.
//            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
//                public void onVideoEnd() {
//                    // Publishers should allow native ads to complete video playback before refreshing
//                    // or replacing them with another ad in the same UI location.
////                refresh.setEnabled(true);
////                videoStatus.setText("Video status: Video playback has ended.");
//                    super.onVideoEnd();
//                }
//            });
//
//            MediaView mediaView = adView.findViewById(R.id.ad_media);
//            ImageView mainImageView = adView.findViewById(R.id.ad_image);
//
//            // Apps can check the VideoController's hasVideoContent property to determine if the
//            // NativeAppInstallAd has a video asset.
//            if (vc.hasVideoContent()) {
//                adView.setMediaView(mediaView);
//                mainImageView.setVisibility(View.GONE);
////            videoStatus.setText(String.format(Locale.getDefault(),
////                    "Video status: Ad contains a %.2f:1 video asset.",
////                    vc.getAspectRatio()));
//            } else {
//                adView.setImageView(mainImageView);
//                mediaView.setVisibility(View.GONE);
//
//                // At least one image is guaranteed.
//                List<NativeAd.Image> images = nativeAd.getImages();
//                mainImageView.setImageDrawable(images.get(0).getDrawable());
//
////            refresh.setEnabled(true);
////            videoStatus.setText("Video status: Ad does not contain a video asset.");
//            }
//
//            adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
//            adView.setBodyView(adView.findViewById(R.id.ad_body));
//            adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
//            adView.setIconView(adView.findViewById(R.id.ad_app_icon));
////        adView.setPriceView(adView.findViewById(R.id.ad_price));
//            adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
////        adView.setStoreView(adView.findViewById(R.id.ad_store));
//            adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
//
//            // Some assets are guaranteed to be in every UnifiedNativeAd.
//            ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
//            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
//            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
//
//            // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
//            // check before trying to display them.
//            if (nativeAd.getIcon() == null) {
//                adView.getIconView().setVisibility(View.GONE);
//            } else {
//                ((ImageView) adView.getIconView()).setImageDrawable(
//                        nativeAd.getIcon().getDrawable());
//                adView.getIconView().setVisibility(View.VISIBLE);
//            }
//
////        if (nativeAd.getPrice() == null) {
////            adView.getPriceView().setVisibility(View.INVISIBLE);
////        } else {
////            adView.getPriceView().setVisibility(View.VISIBLE);
////            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
////        }
//
////        if (nativeAd.getStore() == null) {
////            adView.getStoreView().setVisibility(View.INVISIBLE);
////        } else {
////            adView.getStoreView().setVisibility(View.VISIBLE);
////            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
////        }
//
//            if (nativeAd.getStarRating() == null) {
//                adView.getStarRatingView().setVisibility(View.INVISIBLE);
//            } else {
//                ((RatingBar) adView.getStarRatingView())
//                        .setRating(nativeAd.getStarRating().floatValue());
//                adView.getStarRatingView().setVisibility(View.VISIBLE);
//            }
//
//            if (nativeAd.getAdvertiser() == null) {
//                adView.getAdvertiserView().setVisibility(View.INVISIBLE);
//            } else {
//                ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
//                adView.getAdvertiserView().setVisibility(View.VISIBLE);
//            }
//
//            adView.setNativeAd(nativeAd);
//
//        } catch (Exception e) {
//            FirebaseCrashlytics.getInstance().recordException(e);
//        }
//    }

    /**
     * Creates a request for a new native ad based on the boolean parameters and calls the
     * corresponding "populate" method when one is successfully returned.
     */
    private void refreshAd() {
        try {
//        refresh.setEnabled(false);

            AdLoader adLoader = new AdLoader.Builder(getActivity(), getResources().getString(R.string.admob_small_native_id))
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd nativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();
                            TemplateView template = mView.findViewById(R.id.my_template1);
//                        template.setStyles(styles);
//                        template.setNativeAd(nativeAd);
                            if(nativeAd == null){
                                template.setVisibility(View.GONE);
                            }else {
//                                template.setVisibility(View.VISIBLE);
//                                template.setNativeAd(nativeAd);
                            }
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {

                            // Handle the failure by logging, altering the UI, and so on.
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader.loadAd(new AdRequest.Builder().build());

            AdLoader adLoader2 = new AdLoader.Builder(getActivity(), getResources().getString(R.string.admob_large_native_id))
                    .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                        @Override
                        public void onNativeAdLoaded(NativeAd nativeAd) {
                            // Show the ad.
                            NativeTemplateStyle styles = new
                                    NativeTemplateStyle.Builder().build();
                            TemplateView template = mView.findViewById(R.id.my_template2);
//                        template.setStyles(styles);
//                        template.setNativeAd(nativeAd);
                            if(nativeAd == null){
                                template.setVisibility(View.GONE);
                            }else {
                                template.setVisibility(View.VISIBLE);
                                template.setNativeAd(nativeAd);
                            }
                        }
                    })
                    .withAdListener(new AdListener() {
                        @Override
                        public void onAdFailedToLoad(LoadAdError adError) {

                            // Handle the failure by logging, altering the UI, and so on.
                        }
                    })
                    .withNativeAdOptions(new NativeAdOptions.Builder()
                            // Methods in the NativeAdOptions.Builder class can be
                            // used here to specify individual options settings.
                            .build())
                    .build();
            adLoader2.loadAd(new AdRequest.Builder().build());

//        videoStatus.setText("");

        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }



    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(getActivity());
        adtView.setAdUnitId(getResources().getString(R.string.admob_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(getActivity(), adWidth);
    }
}
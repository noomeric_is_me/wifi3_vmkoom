package vmkoomwifimap.ui.presenter;


import com.google.firebase.crashlytics.FirebaseCrashlytics;

import javax.inject.Inject;

import vmkoomwifimap.adapter.FavouritesAdapter;
import vmkoomwifimap.adapter.ScanResultAdapter;
import vmkoomwifimap.model.wifi.WifiElement;
import vmkoomwifimap.utils.DataBaseAction;

/**
 * Created by Federico
 */
public class DataSetHandler {
    private final DataSetExecutor dataSetExecutor;
    private final FavouritesAdapter favouritesAdapter;
    private final ScanResultAdapter scanResultAdapter;

    @Inject
    public DataSetHandler(DataSetExecutor dataSetExecutor, FavouritesAdapter favouritesAdapter, ScanResultAdapter scanResultAdapter) {
        this.dataSetExecutor = dataSetExecutor;
        this.favouritesAdapter = favouritesAdapter;
        this.scanResultAdapter = scanResultAdapter;
    }

    public void onWifiListReceive() {
        try {
            dataSetExecutor.onWifiListReceive();
            favouritesAdapter.notifyDataSetChanged();
            scanResultAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    public DataBaseAction getDataBaseAction(WifiElement wifiElement) {
        return dataSetExecutor.isSaved(wifiElement)
                ? DataBaseAction.IS_PRESENT
                : DataBaseAction.NOT_PRESENT;
    }

    public void toggleSave(WifiElement wifiElement) {
        dataSetExecutor.toggleSave(wifiElement);
        favouritesAdapter.notifyDataSetChanged();
        scanResultAdapter.notifyDataSetChanged();
    }

    public void scanWifi() {
        dataSetExecutor.wifiNeedToSetEnable();
        dataSetExecutor.clearWifiList();

        scanResultAdapter.notifyDataSetChanged();
        dataSetExecutor.startScan();
    }

    public boolean checkToInitialize() {
        boolean result = dataSetExecutor.wifiNeedToSetEnable();

        if (dataSetExecutor.isWifiListEmpty()) {
            scanWifi();
        }

        return result;
    }
}

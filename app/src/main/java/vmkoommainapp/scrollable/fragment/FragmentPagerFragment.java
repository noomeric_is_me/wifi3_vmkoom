package vmkoommainapp.scrollable.fragment;

import vmkoommainapp.scrollable.BaseFragment;
import ru.noties.scrollable.CanScrollVerticallyDelegate;
import ru.noties.scrollable.OnFlingOverListener;


public abstract class FragmentPagerFragment extends BaseFragment implements CanScrollVerticallyDelegate, OnFlingOverListener {

}

package vmkoomfreewifi.main;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vmkoom.wifimap.connection.hotspot.wifianalyzer.password.anywhere.R;

import vmkoomfreewifi.application.AppController;
import vmkoomfreewifi.services.GsonRequest;
import vmkoomfreewifi.services.ServiceAPI;
import vmkoomfreewifi.services.model.NetworkInformation;
import vmkoomwifimap.fragment.SpeedMapFragment;
import utils.AppUtils;

import static com.thefinestartist.utils.service.ServiceUtil.getWindowManager;

/**
 * Created by VMKoom on 9/27/2017 AD.
 */

public class MapsMarkerActivity extends AppCompatActivity
        implements OnMapReadyCallback {

    public static final String TAG = SpeedMapFragment.class.getSimpleName();
    public static final String LAT_MESSAGE = "LAT_MESSAGE";
    public static final String LNG_MESSAGE = "LNG_MESSAGE";
    private double lat_message;
    private double lng_message;
    private GoogleMap mGoogleMap;
    private FrameLayout adContainerView;
    private AdView adtView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        lat_message = intent.getDoubleExtra(LAT_MESSAGE,0);
        lng_message = intent.getDoubleExtra(LNG_MESSAGE,0);

        // Retrieve the content view that renders the map.
        setContentView(R.layout.map_with_marker);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {}
        });
        //Init admob or facebook
        initAds();
    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        mGoogleMap = googleMap;

        if(lat_message == 0 || lng_message == 0){
            getNetworkInfo();
            return;
        }

        LatLng sydney = new LatLng(lat_message, lng_message);
        googleMap.addMarker(new MarkerOptions().position(sydney).title("Network Here!"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));


//        LatLng sydney = new LatLng(lat, lng);
//        googleMap.addMarker(new MarkerOptions().position(sydney).title("Network Here!"));
//        //        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
    }

//    private com.facebook.ads.AdView adViewfacebook;
//    private NativeAdsManager manager;
//    private NativeAdScrollView nativeAdScrollView;
    private void initAds(){

        if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {
            //Show Admob Ads

            //Banner
//            AdView mAdView = (AdView) findViewById(R.id.admob_banner_view);
//            mAdView.setVisibility(View.VISIBLE);
//            AppUtils.getInstance().showAdsBanner(mAdView);
            adContainerView = findViewById(R.id.ad_view_container);
            adContainerView.setVisibility(View.VISIBLE);
            // Since we're loading the banner based on the adContainerView size, we need to wait until this
            // view is laid out before we can get the width.
            adContainerView.post(new Runnable() {
                @Override
                public void run() {
                    loadBanner();
                }
            });
        } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {
            //Show Facebook Ads

            RelativeLayout adViewContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
//            adViewfacebook = AppUtils.getInstance().showFBAdsBanner(this,adViewContainer);

//            // Initialize a NativeAdsManager and request 5 ads
//            manager = new NativeAdsManager(MapsMarkerActivity.this, AppController.getInstance().getAppContext().getResources().getString(R.string.fb_id_ads_native), 1);
//            manager.setListener(new NativeAdsManager.Listener() {
//                @Override
//                public void onAdsLoaded() {
//                    if(manager.isLoaded()){
//                        nativeAdScrollView = new NativeAdScrollView(MapsMarkerActivity.this, manager,
//                                NativeAdView.Type.HEIGHT_100);
//                        LinearLayout hscrollContainer = (LinearLayout) findViewById(R.id.hscrollContainer);
//                        hscrollContainer.addView(nativeAdScrollView);
//                    }
//                }
//
//                @Override
//                public void onAdError(AdError adError) {
//                    // Ad error callback
//                }
//            });
//            manager.loadAds(NativeAd.MediaCacheFlag.ALL);
        }
    }

    @Override
    public void onBackPressed() {

        try {
            if(AppUtils.ads_interstitial_show_all) {

                if (AppUtils.adsNetworkType != AppUtils.NONE_TYPE) {
                    AppUtils.getInstance().showAdsFullBanner(null);
                }

            }else {

                if (AppUtils.adsNetworkType == AppUtils.ADMOB_ADS_TYPE) {

                    AppUtils.getInstance().showAdmobAdsFullBanner(null);

                } else if (AppUtils.adsNetworkType == AppUtils.FB_ADS_TYPE) {

                    AppUtils.getInstance().showFBAdsFullBanner(null);

                }
            }
        } catch (Exception e) {
            FirebaseCrashlytics.getInstance().recordException(e);
        }

        super.onBackPressed();
    }
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if (adtView != null) {
            adtView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        if (adtView != null) {
            adtView.resume();
        }
    }
    @Override
    public void onDestroy() {
//        if(adViewfacebook != null){
//            adViewfacebook.destroy();
//        }
        if (adtView != null) {
            adtView.destroy();
        }
        super.onDestroy();
    }

    private void getNetworkInfo(){
        String get_networkinfo_url = "http://ip-api.com/json/";
        //Volley
        GsonRequest reqNetworkInfo = ServiceAPI.getNetworkInfo(get_networkinfo_url,new Response.Listener<NetworkInformation>() {
            @Override
            public void onResponse(NetworkInformation response) {

                if(response != null){
                    AppUtils.getInstance().getAppNetworkInfo().setNetworkInformation(response);

                    double lat = Double.parseDouble(response.getLat());
                    double lng = Double.parseDouble(response.getLon());

                    LatLng sydney = new LatLng(lat, lng);
                    mGoogleMap.addMarker(new MarkerOptions().position(sydney).title("Network Here!"));
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        reqNetworkInfo.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqNetworkInfo, TAG);
    }

    private void loadBanner() {
        // Create an ad request.
        adtView = new AdView(this);
        adtView.setAdUnitId(getResources().getString(R.string.admob_banner_id));
        adContainerView.removeAllViews();
        adContainerView.addView(adtView);

        AdSize adSize = getAdSize();
        adtView.setAdSize(adSize);

        AdRequest adRequest = new AdRequest.Builder().build();

        // Start loading the ad in the background.
        adtView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }
}

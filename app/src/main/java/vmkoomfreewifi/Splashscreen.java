package vmkoomfreewifi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import vmkoomfreewifi.main.MainAppActivity_v2;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;

import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.material.snackbar.Snackbar;
import com.thefinestartist.Base;
import com.vmkoom.wifimap.connection.hotspot.wifianalyzer.password.anywhere.R;


import java.util.Objects;

import static android.content.ContentValues.TAG;


public class Splashscreen extends AppCompatActivity {
    CoordinatorLayout coordinatorLayout;
    private static final int LONG_DURATION_MS = 2750;
    LottieAnimationView lottieAnimationView;

    private InterstitialAd mInterstitialAd;
    // Remote Config keys

    private static final String CONFIG_KEY_NATIVE_LARGE_DISPLAY = "display_native_large";
    private static final String CONFIG_KEY_NATIVE_SMALL_DISPLAY = "display_native_small";
    private static final String CONFIG_KEY_FULL_OPEN_APP_DISPLAY = "display_full_open_app";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        coordinatorLayout = findViewById(R.id.cordi);
        lottieAnimationView = findViewById(R.id.animation_view);

//        if (!isOnline(getApplicationContext())) {
//
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    Intent i=new Intent(Splashscreen.this, MainAppActivity_v2.class);
//                    startActivity(i);
//                    finish();
//                    showInterstitial();
//                }
//            }, 3000);
//            Snackbar snackbar2 = Snackbar
//                    .make(coordinatorLayout, "Check internet connection", Snackbar.LENGTH_LONG);
//            snackbar2.show();
//
//
//        } else {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    Intent i=new Intent(Splashscreen.this, MainAppActivity_v2.class);
//                    startActivity(i);
//                    finish();
//                    showInterstitial();
//                }
//            }, 3000);
////            startActivity(new Intent(this, IntroActivity.class));
////            finish();
//        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(Splashscreen.this, MainAppActivity_v2.class);
                startActivity(i);
                finish();
                showInterstitial();
            }
        }, 5000);
//        //Toast.makeText(this, "initialize in Splash", //Toast.LENGTH_SHORT).show();
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Loading , Please wait...", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();

        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, getResources().getString(R.string.admob_interstitial_id), adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        // The mInterstitialAd reference will be null until
                        // an ad is loaded.
                        mInterstitialAd = interstitialAd;

                        Log.i(TAG, "onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error
                        Log.i(TAG, loadAdError.getMessage());
                        mInterstitialAd = null;
                    }
                });
        // Added in Application class //// Added in Application class //Base.initialize(this);
    }


    public void showInterstitial() {

        //show native
        if (mInterstitialAd != null) {
            mInterstitialAd.show(this);
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
//                Toast.makeText(getActivity(), "CONFIG_KEY_NATIVE_LARGE_DISPLAY - True", Toast.LENGTH_SHORT).show();

    }
    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = Objects.requireNonNull(cm).getActiveNetworkInfo();
            return nInfo != null && nInfo.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}